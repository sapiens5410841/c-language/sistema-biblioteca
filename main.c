#include <stdio.h>
#include <string.h>
#include <time.h>

#define MAX_USUARIOS 100
#define MAX_LIVROS 100
#define MAX_EMPRESTIMOS 100

typedef struct {
    int ISBN;
    char nome[100];
    char autor[100];
    float valor;
} Livro;

typedef struct {
    char cpf[12];
    char nome[100];
    int idade;
} Usuario;

typedef struct {
    int ISBNs[10]; // Sup�e-se um m�ximo de 10 livros por empr�stimo
    time_t dataEmprestimo;
    time_t dataDevolucao;
    float multaPorDia;
} Emprestimo;

Livro livros[MAX_LIVROS];
Usuario usuarios[MAX_USUARIOS];
Emprestimo emprestimos[MAX_EMPRESTIMOS];

int numLivros = 0, numUsuarios = 0, numEmprestimos = 0;
float taxaMulta = 0.10; // 10% inicial

void cadastrarLivro() {
    system("cls");
    if (numLivros >= MAX_LIVROS) {
        printf("Limite de livros atingido!\n");
        return;
    }
    printf("Digite o ISBN do livro: ");
    scanf("%d", &livros[numLivros].ISBN);
    printf("Digite o nome do livro: ");
    scanf(" %[^\n]", livros[numLivros].nome);
    printf("Digite o autor do livro: ");
    scanf(" %[^\n]", livros[numLivros].autor);
    printf("Digite o valor do livro: ");
    scanf("%f", &livros[numLivros].valor);
    numLivros++;
    printf("Livro cadastrado com sucesso!\n");
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void visualizarLivros() {
    system("cls");
    if (numLivros == 0) {
        printf("Nenhum livro cadastrado!\n");
        return;
    }
    printf("Livros cadastrados:\n");
    for (int i = 0; i < numLivros; i++) {
        printf("%d. ISBN: %d, Nome: %s, Autor: %s, Valor: %.2f\n", i + 1, livros[i].ISBN, livros[i].nome, livros[i].autor, livros[i].valor);
    }
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void editarLivro() {
    system("cls");
    int isbn, found = 0;
    printf("Digite o ISBN do livro que deseja editar: ");
    scanf("%d", &isbn);
    for (int i = 0; i < numLivros; i++) {
        if (livros[i].ISBN == isbn) {
            printf("Editando livro %s:\n", livros[i].nome);
            printf("Novo ISBN (anterior %d): ", livros[i].ISBN);
            scanf("%d", &livros[i].ISBN);
            printf("Novo nome (anterior %s): ", livros[i].nome);
            scanf(" %[^\n]", livros[i].nome);
            printf("Novo autor (anterior %s): ", livros[i].autor);
            scanf(" %[^\n]", livros[i].autor);
            printf("Novo valor (anterior %.2f): ", livros[i].valor);
            scanf("%f", &livros[i].valor);
            found = 1;
            printf("Livro editado com sucesso!\n");
            break;
        }
    }
    if (!found) {
        printf("ISBN nao encontrado!\n");
    }
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void deletarLivro() {
    system("cls");
    int isbn, found = 0;
    printf("Digite o ISBN do livro que deseja deletar: ");
    scanf("%d", &isbn);
    for (int i = 0; i < numLivros; i++) {
        if (livros[i].ISBN == isbn) {
            for (int j = i; j < numLivros - 1; j++) {
                livros[j] = livros[j + 1];
            }
            numLivros--;
            found = 1;
            printf("Livro deletado com sucesso!\n");
            break;
        }
    }
    if (!found) {
        printf("ISBN nao encontrado!\n");
    }
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void cadastrarUsuario() {
    system("cls");
    if (numUsuarios >= MAX_USUARIOS) {
        printf("Limite de usuarios atingido!\n");
        return;
    }
    printf("Digite o CPF do usuario (11 caracteres): ");
    scanf("%s", usuarios[numUsuarios].cpf);
    printf("Digite o nome do usuario: ");
    scanf(" %[^\n]", usuarios[numUsuarios].nome);
    printf("Digite a idade do usuario: ");
    scanf("%d", &usuarios[numUsuarios].idade);
    numUsuarios++;
    printf("Usuario cadastrado com sucesso!\n");
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void visualizarUsuarios() {
    system("cls");
    if (numUsuarios == 0) {
        printf("Nenhum usuario cadastrado!\n");
        return;
    }
    printf("Usuarios cadastrados:\n");
    for (int i = 0; i < numUsuarios; i++) {
        printf("%d. CPF: %s, Nome: %s, Idade: %d\n", i + 1, usuarios[i].cpf, usuarios[i].nome, usuarios[i].idade);
    }
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void editarUsuario() {
    system("cls");
    char cpf[12];
    int found = 0;
    printf("Digite o CPF do usuario que deseja editar: ");
    scanf("%s", cpf);
    for (int i = 0; i < numUsuarios; i++) {
        if (strcmp(usuarios[i].cpf, cpf) == 0) {
            printf("Editando usuario %s:\n", usuarios[i].nome);
            printf("Novo CPF (anterior %s): ", usuarios[i].cpf);
            scanf("%s", usuarios[i].cpf);
            printf("Novo nome (anterior %s): ", usuarios[i].nome);
            scanf(" %[^\n]", usuarios[i].nome);
            printf("Nova idade (anterior %d): ", usuarios[i].idade);
            scanf("%d", &usuarios[i].idade);
            found = 1;
            printf("Usuario editado com sucesso!\n");
            break;
        }
    }
    if (!found) {
        printf("CPF nao encontrado!\n");
    }
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void deletarUsuario() {
    system("cls");
    char cpf[12];
    int found = 0;
    printf("Digite o CPF do usuario que deseja deletar: ");
    scanf("%s", cpf);
    for (int i = 0; i < numUsuarios; i++) {
        if (strcmp(usuarios[i].cpf, cpf) == 0) {
            for (int j = i; j < numUsuarios - 1; j++) {
                usuarios[j] = usuarios[j + 1];
            }
            numUsuarios--;
            found = 1;
            printf("Usuario deletado com sucesso!\n");
            break;
        }
    }
    if (!found) {
        printf("CPF nao encontrado!\n");
    }
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void visualizarEmprestimos() {
    system("cls");
    if (numEmprestimos == 0) {
        printf("Nenhum emprestimo cadastrado!\n");
        return;
    }
    printf("Emprestimos cadastrados:\n");
    char buf[20];
    for (int i = 0; i < numEmprestimos; i++) {
        printf("Emprestimo %d:\n", i + 1);
        printf("  Livros emprestados: ");
        for (int j = 0; j < 10 && emprestimos[i].ISBNs[j] != 0; j++) {
            printf("%d ", emprestimos[i].ISBNs[j]);
        }
        printf("\n");
        strftime(buf, 20, "%Y-%m-%d %H:%M:%S", localtime(&emprestimos[i].dataEmprestimo));
        printf("  Data do emprestimo: %s\n", buf);
        strftime(buf, 20, "%Y-%m-%d %H:%M:%S", localtime(&emprestimos[i].dataDevolucao));
        printf("  Data de devolucao: %s\n", buf);
        printf("  Multa por dia: %.2f\n", emprestimos[i].multaPorDia);
    }
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void cadastrarEmprestimo() {
    system("cls");
    if (numEmprestimos >= MAX_EMPRESTIMOS) {
        printf("Limite de emprestimos atingido!\n");
        return;
    }
    printf("Digite o numero de livros do emprestimo: ");
    int numLivrosEmprestimo;
    scanf("%d", &numLivrosEmprestimo);
    for (int i = 0; i < numLivrosEmprestimo; i++) {
        printf("Digite o ISBN do livro %d: ", i + 1);
        scanf("%d", &emprestimos[numEmprestimos].ISBNs[i]);
    }
    emprestimos[numEmprestimos].dataEmprestimo = time(NULL);
    printf("Digite o numero de dias de emprestimo: ");
    int diasEmprestimo;
    scanf("%d", &diasEmprestimo);
    emprestimos[numEmprestimos].dataDevolucao = emprestimos[numEmprestimos].dataEmprestimo + (diasEmprestimo * 86400);

    // Calculando a multa por dia
    emprestimos[numEmprestimos].multaPorDia = 0;
    for (int i = 0; i < numLivrosEmprestimo; i++) {
        for (int j = 0; j < numLivros; j++) {
            if (emprestimos[numEmprestimos].ISBNs[i] == livros[j].ISBN) {
                emprestimos[numEmprestimos].multaPorDia += livros[j].valor * taxaMulta;
                break;
            }
        }
    }
    numEmprestimos++;
    printf("Emprestimo cadastrado com sucesso!\n");
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

void modificarMulta() {
    system("cls");
    printf("Digite a nova taxa de multa por dia (formato decimal, ex: 0.20 para 20%%): ");
    scanf("%f", &taxaMulta);
    printf("Taxa de multa modificada para %.2f%%\n", taxaMulta * 100);
    printf("Pressione ENTER para continuar.\n");
    getchar();
    getchar();
}

int main() {
    int opcao;
    do {
        system("cls");
        printf("\nBiblioteca - Menu Principal\n");
        printf("1. Cadastrar novo usuario\n");
        printf("2. Cadastrar novo livro\n");
        printf("3. Cadastrar novo emprestimo\n");
        printf("4. Modificar taxa de multa por dia\n");
        printf("5. Visualizar todos os livros\n");
        printf("6. Visualizar todos os usuarios\n");
        printf("7. Editar livro\n");
        printf("8. Editar usuario\n");
        printf("9. Deletar livro\n");
        printf("10. Deletar usuario\n");
        printf("11. Visualizar emprestimos\n");
        printf("12. Sair\n");
        printf("Escolha uma opcao: ");
        scanf("%d", &opcao);

        switch (opcao) {
            case 1:
                cadastrarUsuario();
                break;
            case 2:
                cadastrarLivro();
                break;
            case 3:
                cadastrarEmprestimo();
                break;
            case 4:
                modificarMulta();
                break;
            case 5:
                visualizarLivros();
                break;
            case 6:
                visualizarUsuarios();
                break;
            case 7:
                editarLivro();
                break;
            case 8:
                editarUsuario();
                break;
            case 9:
                deletarLivro();
                break;
            case 10:
                deletarUsuario();
                break;
            case 11:
                visualizarEmprestimos();
                break;
            case 12:
                printf("Saindo do sistema...\n");
                break;
            default:
                printf("Opcao invalida!\n");
        }
    } while (opcao != 12);

    return 0;
}
